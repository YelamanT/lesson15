import kz.kaspi.lesson15.entity.Client;
import kz.kaspi.lesson15.entity.CustomerOrder;
import kz.kaspi.lesson15.entity.Manufacturer;
import kz.kaspi.lesson15.entity.Product;
import kz.kaspi.lesson15.services.ClientService;
import kz.kaspi.lesson15.services.CustomerOrderService;
import kz.kaspi.lesson15.services.ManufacturerService;
import kz.kaspi.lesson15.services.ProductService;
import kz.kaspi.lesson15.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

    ClientService clientService = new ClientService();
    CustomerOrderService orderService = new CustomerOrderService();
    ManufacturerService mService= new ManufacturerService();
    ProductService pService = new ProductService();


    public static void main(String[] args) { new Main().go();}

    public void go () {
        List<Product> list = getTopProducts();
        list.stream().map(e->e.getId()).forEach(System.out::println);
//        deleteProduct(26L);
    }

    public static List <Product> getTopProducts() {
        List <Product> list;
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();

        String hql = ("From Product");
        Query query = session.createQuery(hql);
        list = query.getResultList();

        list = list.stream()
                .sorted(Comparator.comparingInt(e->e.getOrders().size()))
                .collect(Collectors.toList());
        Collections.reverse(list);

        list.subList(5,list.size()).clear();

        return list;
    }

    public void insertClient (Client client) {
        clientService.persist(client);
    }

    public void insertOrder (CustomerOrder order, List <Product> list) {
        order.getProductList().addAll(list);
        orderService.persist(order);
    }
    public void insertManufacturer (Manufacturer manu) {
        mService.persist(manu);
    }
    public void insertProduct (Product product) {
        pService.persist(product);
    }

    public Client findClient (Long id) {
        Client client = clientService.findById(id);
        return client;
    }
    public Product findProduct (Long id) {
        Product product = pService.findById(id);
        return product;
    }
    public Manufacturer findManufacturer (Long id) {
        Manufacturer manu = mService.findById(id);
        return manu;
    }
    public CustomerOrder findOrder (Long id) {
        CustomerOrder order = orderService.findById(id);
        return order;
    }
    public void insertProductsToAnOrder (List <Product> list,Long id) {
        CustomerOrder order= findOrder(id);
        order.getProductList().addAll(list);
    }


    public void updateClient (Client client) {
        clientService.update(client);
    }
    public void updateProduct (Product product) {
        pService.update(product);
    }
    public void updateManufacturer (Manufacturer manu) {
        mService.update(manu);
    }
    public void updateOrder (CustomerOrder order) {
        orderService.update(order);
    }

    public void deleteClient (Long id) {
        clientService.delete(id);
    }
    public void deleteManufacturer (Long id) {
        mService.delete(id);
    }
    public void deleteOrder (Long id) {
        orderService.delete(id);
    }
    public void deleteProduct (Long id) {
        pService.delete(id);
    }






}
