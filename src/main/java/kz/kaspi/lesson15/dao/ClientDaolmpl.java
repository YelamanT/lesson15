package kz.kaspi.lesson15.dao;

import kz.kaspi.lesson15.entity.Client;

import java.util.List;

public class ClientDaolmpl  extends BaseDao <Client, Long>{

    @Override
    public void persist(Client entity) {
        getCurrentSession().save(entity);
    }

    @Override
    public void update(Client entity) {
        getCurrentSession().update(entity);
    }

    @Override
    public Client findById(Long id) {
        return getCurrentSession().get(Client.class, id);
    }

    @Override
    public void delete(Client entity) {
        getCurrentSession().delete(entity);
    }

    @Override
    public List<Client> findAll() {
        return getCurrentSession().
                createQuery("from Client").list();
    }

    @Override
    public void deleteAll() {
        findAll().forEach(d -> delete(d));
    }


}
