package kz.kaspi.lesson15.dao;



import kz.kaspi.lesson15.entity.CustomerOrder;

import java.util.List;

public class CustomerOrderDaolmpl extends BaseDao <CustomerOrder, Long>{

    @Override
    public void persist(CustomerOrder entity) {
        getCurrentSession().save(entity);
    }

    @Override
    public void update(CustomerOrder entity) {
        getCurrentSession().update(entity);
    }

    @Override
    public CustomerOrder findById(Long id) {
        return getCurrentSession().get(CustomerOrder.class, id);
    }

    @Override
    public void delete(CustomerOrder entity) {
        getCurrentSession().delete(entity);
    }

    @Override
    public List<CustomerOrder> findAll() {
        return getCurrentSession().
                createQuery("from CustomerOrder").list();
    }

    @Override
    public void deleteAll() {
        findAll().forEach(d -> delete(d));
    }
}
