package kz.kaspi.lesson15.dao;

import kz.kaspi.lesson15.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;

public abstract class BaseDao<T, ID> implements ClientDao<T, ID>,
        CutomerOrderDao <T, ID>, ProductDao <T, ID>, ManufacturerDao <T, ID> {
    private Session currentSession;

    private Transaction currentTransaction;

    public Session openSession() {
        currentSession = HibernateUtil.getSessionFactory().openSession();
        currentTransaction = currentSession.beginTransaction();
        return currentSession;
    }

    public void closeSession() {
        currentTransaction.commit();
        currentSession.close();
    }

    public Session getCurrentSession() {
        return currentSession;
    }


}
