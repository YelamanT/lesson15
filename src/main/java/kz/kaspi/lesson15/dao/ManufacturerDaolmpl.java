package kz.kaspi.lesson15.dao;

import kz.kaspi.lesson15.entity.Manufacturer;


import java.util.List;

public class ManufacturerDaolmpl extends BaseDao <Manufacturer, Long>{
    @Override
    public void persist(Manufacturer entity) {
        getCurrentSession().save(entity);
    }

    @Override
    public void update(Manufacturer entity) {
        getCurrentSession().update(entity);
    }

    @Override
    public Manufacturer findById(Long id) {
        return getCurrentSession().get(Manufacturer.class, id);
    }

    @Override
    public void delete(Manufacturer entity) {
        getCurrentSession().delete(entity);
    }

    @Override
    public List<Manufacturer> findAll() {
        return getCurrentSession().
                createQuery("from Manufacturer").list();
    }

    @Override
    public void deleteAll() {
        findAll().forEach(d -> delete(d));
    }
}
