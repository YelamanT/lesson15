package kz.kaspi.lesson15.dao;

import kz.kaspi.lesson15.entity.Product;
import kz.kaspi.lesson15.entity.Product;

import java.util.List;

public class ProductDaolmpl extends BaseDao <Product,Long>{

    @Override
    public void persist(Product entity) {
        getCurrentSession().save(entity);
    }

    @Override
    public void update(Product entity) {
        getCurrentSession().update(entity);
    }

    @Override
    public Product findById(Long id) {
        return getCurrentSession().get(Product.class, id);
    }

    @Override
    public void delete(Product entity) {
        getCurrentSession().delete(entity);
    }

    @Override
    public List<Product> findAll() {
        return getCurrentSession().
                createQuery("from Product").getResultList();
    }

    @Override
    public void deleteAll() {
        findAll().forEach(d -> delete(d));
    }
}
