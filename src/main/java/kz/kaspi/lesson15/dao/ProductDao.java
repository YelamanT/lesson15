package kz.kaspi.lesson15.dao;

import org.hibernate.Session;

import java.util.List;

public interface ProductDao <T, ID>{
    public Session openSession();
    public void closeSession();

    public void persist(T entity);

    public void update(T entity);

    public T findById(ID id);

    public void delete(T entity);

    public List<T> findAll();

    public void deleteAll();
}
