package kz.kaspi.lesson15.entity;

import javax.persistence.*;
import java.util.ArrayList;

import java.util.List;


@Entity
public class Product {

    @Id
    @GeneratedValue (strategy = GenerationType.AUTO)
    private Long product_id;

    private String name;

    private int price;

    @ManyToMany(mappedBy = "productList")
    private List<CustomerOrder> orders = new ArrayList<>();

    @ManyToOne (targetEntity = Manufacturer.class)
    @JoinColumn(name="manufacturer_id", referencedColumnName = "id")
    private Manufacturer manufacturerId;

    public Long getId() {
        return product_id;
    }

    public void setId(Long id) {
        this.product_id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public List<CustomerOrder> getOrders() {
        return orders;
    }

    public void setOrders(List<CustomerOrder> employees) {
        this.orders = employees;
    }

    public Manufacturer getManufacturerId() {
        return manufacturerId;
    }

    public void setManufacturerId(Manufacturer manufacturerId) {
        this.manufacturerId = manufacturerId;
    }
}
