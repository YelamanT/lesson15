package kz.kaspi.lesson15.entity;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;

import java.util.List;


@Entity
public class CustomerOrder {

    @Id
    @GeneratedValue (strategy = GenerationType.AUTO)
    private Long order_id;

    @ManyToOne (targetEntity = Client.class)
    @JoinColumn(name="client_id", referencedColumnName = "id")
    private Client clientId;

    private LocalDateTime order_time;

    @ManyToMany(cascade = { CascadeType.ALL })
    @Fetch(FetchMode.JOIN)
    @JoinTable(
            name = "order_products",
            joinColumns = { @JoinColumn(name = "order_id") },
            inverseJoinColumns = { @JoinColumn(name = "product_id") }
    )
    List<Product> productList = new ArrayList<>();

    public Long getId() {
        return order_id;
    }

    public List<Product> getProductList() {
        return productList;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }

    public void setId(Long id) {
        this.order_id = id;
    }

    public Client getClientId() {
        return clientId;
    }

    public void setClientId(Client clientId) {
        this.clientId = clientId;
    }

    public LocalDateTime getOrder_time() {
        return order_time;
    }

    public void setOrder_time(LocalDateTime order_time) {
        this.order_time = order_time;
    }


}
