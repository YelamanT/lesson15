package kz.kaspi.lesson15.services;

import kz.kaspi.lesson15.dao.ManufacturerDao;
import kz.kaspi.lesson15.dao.ManufacturerDaolmpl;
import kz.kaspi.lesson15.dao.CutomerOrderDao;

import kz.kaspi.lesson15.entity.CustomerOrder;
import kz.kaspi.lesson15.entity.Manufacturer;

import java.util.List;


public class ManufacturerService {
    private static ManufacturerDao<Manufacturer, Long> manufacturerDao;

    public ManufacturerService() {
        manufacturerDao = new ManufacturerDaolmpl();
    }


    public void persist(Manufacturer entity) {
        manufacturerDao.openSession();
        manufacturerDao.persist(entity);
        manufacturerDao.closeSession();
    }

    public Manufacturer findById(Long id) {
        manufacturerDao.openSession();
        Manufacturer manuf = manufacturerDao.findById(id);
        manufacturerDao.closeSession();
        return manuf;
    }

    public void delete(Long id) {
        manufacturerDao.openSession();
        Manufacturer manuf = manufacturerDao.findById(id);

        manufacturerDao.delete(manuf);
        manufacturerDao.closeSession();
    }

    public void update(Manufacturer manuf) {
        manufacturerDao.openSession();
        manufacturerDao.update(manuf);
        manufacturerDao.closeSession();
    }

    public void deleteAll() {
        manufacturerDao.openSession();
        manufacturerDao.deleteAll();
        manufacturerDao.closeSession();
    }

    public List<Manufacturer> findAll () {
        manufacturerDao.openSession();
        List <Manufacturer> list = manufacturerDao.findAll();
        manufacturerDao.closeSession();
        return list;
    }
}
