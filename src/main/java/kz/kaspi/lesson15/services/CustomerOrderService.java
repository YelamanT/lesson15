package kz.kaspi.lesson15.services;


import kz.kaspi.lesson15.dao.CustomerOrderDaolmpl;
import kz.kaspi.lesson15.dao.CutomerOrderDao;
import kz.kaspi.lesson15.entity.CustomerOrder;

import java.util.List;


public class CustomerOrderService {
    
    private static CutomerOrderDao<CustomerOrder, Long> orderDao;

    public CustomerOrderService() {
        orderDao = new CustomerOrderDaolmpl();
    }


    public void persist(CustomerOrder entity) {
        orderDao.openSession();
        orderDao.persist(entity);
        orderDao.closeSession();
    }

    public CustomerOrder findById(Long id) {
        orderDao.openSession();
        CustomerOrder cOrder = orderDao.findById(id);
        orderDao.closeSession();
        return cOrder;
    }

    public void delete(Long id) {
        orderDao.openSession();
        CustomerOrder cOrder = orderDao.findById(id);

        orderDao.delete(cOrder);
        orderDao.closeSession();
    }

    public void update(CustomerOrder cOrder) {
        orderDao.openSession();
        orderDao.update(cOrder);
        orderDao.closeSession();
    }

    public void deleteAll() {
        orderDao.openSession();
        orderDao.deleteAll();
        orderDao.closeSession();
    }

    public List<CustomerOrder> findAll () {
        orderDao.openSession();
        List <CustomerOrder> list = orderDao.findAll();
        orderDao.closeSession();
        return list;
    }
}
