package kz.kaspi.lesson15.services;


import kz.kaspi.lesson15.dao.CutomerOrderDao;
import kz.kaspi.lesson15.dao.ProductDao;
import kz.kaspi.lesson15.dao.ProductDaolmpl;
import kz.kaspi.lesson15.entity.CustomerOrder;
import kz.kaspi.lesson15.entity.Product;

import java.util.List;


public class ProductService {
    private static ProductDao<Product, Long> productDao;

    public ProductService() {
        productDao = new ProductDaolmpl();
    }


    public void persist(Product entity) {
        productDao.openSession();
        productDao.persist(entity);
        productDao.closeSession();
    }

    public Product findById(Long id) {
        productDao.openSession();
        Product product = productDao.findById(id);
        productDao.closeSession();
        return product;
    }

    public void delete(Long id) {
        productDao.openSession();
        Product product = productDao.findById(id);

        productDao.delete(product);
        productDao.closeSession();
    }

    public void update(Product product) {
        productDao.openSession();
        productDao.update(product);
        productDao.closeSession();
    }

    public void deleteAll() {
        productDao.openSession();
        productDao.deleteAll();
        productDao.closeSession();
    }
    public List<Product> findAll () {
        productDao.openSession();
        List <Product> list = productDao.findAll();
        productDao.closeSession();
        return list;
    }
}
