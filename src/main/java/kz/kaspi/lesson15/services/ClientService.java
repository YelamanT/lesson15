package kz.kaspi.lesson15.services;


import kz.kaspi.lesson15.dao.ClientDao;
import kz.kaspi.lesson15.dao.ClientDaolmpl;
import kz.kaspi.lesson15.entity.Client;
import kz.kaspi.lesson15.entity.CustomerOrder;

import java.util.List;


public class ClientService{

    private static ClientDao<Client, Long> clientDao;

    public ClientService() {
        clientDao = new ClientDaolmpl();
    }


    public void persist(Client entity) {
        clientDao.openSession();
        clientDao.persist(entity);
        clientDao.closeSession();
    }

    public Client findById(Long id) {
        clientDao.openSession();
        Client client = clientDao.findById(id);
        clientDao.closeSession();
        return client;
    }

    public void delete(Long id) {
        clientDao.openSession();
        Client client = clientDao.findById(id);

        clientDao.delete(client);
        clientDao.closeSession();
    }

    public void update(Client client) {
        clientDao.openSession();
        clientDao.update(client);
        clientDao.closeSession();
    }

    public void deleteAll() {
        clientDao.openSession();
        clientDao.deleteAll();
        clientDao.closeSession();
    }

    public List<Client> findAll () {
        clientDao.openSession();
        List <Client> list = clientDao.findAll();
        clientDao.closeSession();
        return list;
    }


}
